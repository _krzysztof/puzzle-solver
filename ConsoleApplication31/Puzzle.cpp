#include <iostream>
#include <vector>
#include <queue>
#include <unordered_map>
#include <algorithm>
#include <math.h>
using namespace std;

class Puzzle
{
	int element[9];
	int Znajdz_w_Puzzle(Puzzle Puzzle)
{
	int i;
	for (i = 0; i<9; i++)
	{
		if (Puzzle.element[i] == 0)
			return(i + 1);
	}
	return(0);
}

public:
	typedef int pozycja;
	typedef int ruch;
	typedef int cykl;
	static const pozycja blad = 0, ok = 1;
	static const ruch gora = 0, dol = 1, lewo = 2, prawo = 3;
	Puzzle() { for (int i = 0; i < 9; i++) element[i] = i; }
	pozycja Puzzle::w_gore()
	{
		int num;
		num = Znajdz_w_Puzzle(*this);
		if (num>3)
		{
			this->element[num - 1] = this->element[num - 4];
			this->element[num - 4] = 0;
			return(Puzzle::ok);
		}
		else
			return(Puzzle::blad);
	}
	pozycja Puzzle::w_dol()
	{
		int num;
		num = Znajdz_w_Puzzle(*this);
		if (num>0 && num<7)
		{
			this->element[num - 1] = this->element[num + 2];
			this->element[num + 2] = 0;
			return(Puzzle::ok);
		}
		else
			return(Puzzle::blad);
	}
	pozycja Puzzle::w_lewo()
	{
		int num;
		num = Znajdz_w_Puzzle(*this);
		if (num % 3 != 1 && num != 0)
		{
			this->element[num - 1] = this->element[num - 2];
			this->element[num - 2] = 0;
			return(Puzzle::ok);
		}
		else
			return(Puzzle::blad);
	}
	pozycja Puzzle::w_prawo()
	{
		int num;
		num = Znajdz_w_Puzzle(*this);
		if (num % 3 != 0)
		{
			this->element[num - 1] = this->element[num];
			this->element[num] = 0;
			return(Puzzle::ok);
		}
		else
			return(Puzzle::blad);
	}
	pozycja Puzzle::Ruch(Puzzle::ruch param)
	{
		if (param == 0) return(this->w_gore());
		else if (param == 1)return(this->w_dol());
		else if (param == 2)return(this->w_lewo());
		else if (param == 3)return(this->w_prawo());
		else return Puzzle::blad;
	}
	pozycja Puzzle::Ustaw_Puzzle()
	{
		int i, j;
		int czyByl[9];
		bool ok;
		for (i = 0; i<3; i++)
		{
			for (j = 0; j<3; j++)
			{	
				ok = false;
				while (ok != true)
				{	
					ok = true;
					cout << endl << "Rz�d:" << i + 1 << endl << "Kolumna:" << j + 1 << endl;
					cin >> (this->element[3 * i + j]);
					czyByl[3 * i + j] = this->element[3 * i + j];

					if (3 * i + j > 0) // zeby nie porownywac pierwszego
					{
						for (int k = 0; k < 3 * i + j; k++)
						{
							if (czyByl[k] == czyByl[3 * i + j])
							{
								cout << "Ta cyfra juz byla!" << endl;
								ok = false;
							}
						}
					}
				}				
			}
		}
		return(Puzzle::ok);
	}
	pozycja Puzzle::Ustaw_koncowe_Puzzle()
	{
		int i, j;
		int k = 1;
		for (i = 0; i<3; i++)
		{
			for (j = 0; j<3; j++)
			{
				if (i == 2 && j == 2)
					(this->element[3 * i + j]) = 0;
				else
					(this->element[3 * i + j]) = k++;

			}
		}
		return(Puzzle::ok);
	}
	void drukuj()
	{
		int i;
		for (i = 0; i<3; i++)
		{
			if (this->element[i] != 0)
				cout << " " << this->element[i];
			else
				cout << "  ";
		}
		cout << " " << endl << endl;
		for (i = 3; i<6; i++)
		{
			if (this->element[i] != 0)
				cout << " " << this->element[i];
			else
				cout << "  ";
		}
		cout << " " << endl << endl;
		for (i = 6; i<9; i++)
		{
			if (this->element[i] != 0)
				cout << " " << this->element[i];
			else
				cout << " ";
		}
		cout << "  " << endl;
	}

	static vector<Puzzle> Znajdz_rozwiazanie(Puzzle&, Puzzle&);
	static cykl koduj(Puzzle&);
	static Puzzle odkoduj(cykl);

	bool operator == (Puzzle& Table)
	{
		int i;
		for (i = 0; i < 9; i++)
		{
			if (this->element[i] != Table.element[i])
				return(false);
		}
		return(true);
	}

	bool operator != (Puzzle &Table)
	{
		return(!(*this == Table));
	}
};

int Puzzle::koduj(Puzzle &Puzzle)
{
	int i;
	int PuzzleID = 0;
	for (i = 0; i < 9; i++)
		PuzzleID = PuzzleID + Puzzle.element[i] * pow(10, i);
	return(PuzzleID);
}

Puzzle Puzzle::odkoduj(cykl cykl)
{
	Puzzle Odkoduj;
	for (int i = 0; i < 9; i++)
	{
		Odkoduj.element[i] = cykl % 10;
		cykl = cykl / 10;
	}
	return(Odkoduj);
}

vector<Puzzle> Puzzle::Znajdz_rozwiazanie(Puzzle &Poczatek, Puzzle &Koniec)
{
	vector<Puzzle> Rozwiazanie;
	if (Poczatek == Koniec)
	{
		Rozwiazanie.push_back(Koniec);
		return(Rozwiazanie);
	}
	queue <int> otwarty;
	unordered_map<cykl, cykl> zamkniety;
	const Puzzle::cykl KoniecCykl = koduj(Koniec);
	otwarty.push(koduj(Poczatek));
	zamkniety[koduj(Poczatek)] = 0;
	bool czyRozwiazanie = false;
	while (czyRozwiazanie == false && otwarty.empty() == false)
	{
		Puzzle Node = odkoduj(otwarty.front()), NewNode;
		for (int ruch_i = 0; ruch_i < 4; ruch_i++)
		{
			NewNode = Node;
			if (NewNode.Ruch(ruch_i) == Puzzle::ok)
			{
				int nowyCykl = koduj(NewNode);
				if (zamkniety.find(nowyCykl) == zamkniety.end())
				{
					zamkniety[nowyCykl] = otwarty.front();
					otwarty.push(nowyCykl);
				}
				if (NewNode == Koniec)
				{
					czyRozwiazanie = true;
					break;
				}
			}
		}
		otwarty.pop();
	}
	if (czyRozwiazanie == false)
		return Rozwiazanie;
	Puzzle::cykl Rozwiazanie_droga = KoniecCykl;
	while (Rozwiazanie_droga != 0)
	{
		Rozwiazanie.insert(Rozwiazanie.begin(), odkoduj(Rozwiazanie_droga));
		Rozwiazanie_droga = zamkniety[Rozwiazanie_droga];
	}
	return(Rozwiazanie);
}


int main()
{
	setlocale(LC_ALL, "");
	Puzzle Poczatek, Koniec;
	cout << "Podaj cyfre od 0 do 8, przy czym 0 odpowiada pustemu miejscu" << endl << endl;
	cout << "Uk�ad pocz�tkowy:" << endl;
	Poczatek.Ustaw_Puzzle();
	cout << "Uk�ad ko�cowy:" << endl << endl;
	Koniec.Ustaw_koncowe_Puzzle();
	system("cls");
	cout << endl << "Stan pocz�tkowy:" << endl << endl;
	Poczatek.drukuj();
	cout << endl << endl << "Cel:" << endl << endl;
	Koniec.drukuj();
	cout << "_______" << endl << endl;
	vector<Puzzle> Rozwiazanie;
	Rozwiazanie = Puzzle::Znajdz_rozwiazanie(Poczatek, Koniec);
	if (Rozwiazanie.empty() == false)
	{
		for (int i = 0; i < Rozwiazanie.size(); i++)
		{
			cout << endl << endl << "Krok " << i << ": " << endl << endl;
			Rozwiazanie[i].drukuj();
		}
	}
	else
		cout << "Nie ma rozwi�zania" << endl;

	return 0;
}